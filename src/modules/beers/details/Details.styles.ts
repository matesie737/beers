import styled from "@emotion/styled"
import { CircularProgress as BaseCircularProgress } from "@mui/material"

export const Container = styled("div")({
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
  alignItems: "center",
  height: "calc(100vh - 90px)",
  width: "100%",
})

export const CircularProgress = styled(BaseCircularProgress)({
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%,-50%)",
})

export const NotFound = styled("div")({
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%,-50%)",
  fontSize: "20px",
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
})

export const ImageContainer = styled("div")({
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
})

export const Shadow = styled("div")({
  width: "40%",
  zIndex: "-1000",
  height: "7%",
  position: "relative",
  top: "-5%",
  borderRadius: "50%",
  backgroundColor: "#BEBEBE",
  boxShadow: "0px 0px 20px 10px #BEBEBE",
})

export const RightContainer = styled("div")({
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
})

export const UpperContainer = styled("div")({
  width: "90%",
  margin: "20px 0px",
})

export const Title = styled("p")({
  textAlign: "center",
  fontSize: "30px",
  fontWeight: "600",
})

export const Tagline = styled("p")({
  fontSize: "20px",
  textAlign: "center",
  color: "#A4A4A4",
})

export const Description = styled("div")({
  fontSize: "16px",
  textAlign: "justify",
  color: "#545454",
})

export const BottomContainer = styled("div")({
  display: "flex",
})

export const ParametrContainer = styled("div")({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
})

export const AbvIbu = styled("div")({
  display: "flex",
  margin: "10px",
  backgroundColor: "#F8F8F8",
  border: "2px solid #C4C4C4",
  borderRadius: "5px",
  alignItems: "center",
  justifyContent: "center",
})

export const IngridientsContainer = styled("div")({
  width: "80%",
  backgroundColor: "#F8F8F8",
  border: "2px solid #C4C4C4",
  borderRadius: "5px",
  padding: "15px",
})

export const IngredientTypeTitle = styled("div")({
  fontWeight: "600",
})

export const Amount = styled("p")({
  margin: "0px 5px",
  color: "#A2A2A2",
})

export const Name = styled("p")({
  margin: "0px 5px",
  fontWeight: "500",
})

export const Time = styled("p")({
  margin: "0px 5px",
  color: "#A2A2A2",
})

export const Ingredient = styled("div")({
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
  margin: "5px 0px 5px 10px",
})

export const Attribute = styled("p")({
  margin: "0px",
})

export const C = styled("div")({})
