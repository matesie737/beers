import { useGetBeersQuery } from "api"
import {
  BeerList,
  BeerListContainer,
  Container,
  PaginationContainer,
  PageNumber,
  PlaceHolder,
  CircularProgress,
  NotFound,
} from "./Beers.styles"
import { useEffect, useState } from "react"
import { BeerCard } from "components"
import { IconButton } from "@mui/material"
import { left, right, sad } from "utils"
import { useLocation } from "react-router-dom"
import { Beer } from "types"

const Beers = () => {
  const [page, setPage] = useState<number>(1)
  const [searchString, setSearchString] = useState<string | null>(null)
  const { search } = useLocation()
  const { data, isLoading, refetch, isFetching } = useGetBeersQuery({
    page: page,
    search: searchString,
  })

  const handlePrev = () => {
    if (page !== 1) setPage(page - 1)
  }
  const handleNext = () => {
    if (data && data.length === 12) setPage(page + 1)
  }

  useEffect(() => {
    refetch()
  }, [page, refetch])

  useEffect(() => {
    const searchTerm = new URLSearchParams(search)
    setSearchString(searchTerm.get("search"))
    setPage(1)
    refetch()
  }, [search, refetch])

  return (
    <Container>
      <BeerListContainer>
        {isLoading || isFetching ? (
          <CircularProgress />
        ) : !data || data.length === 0 ? (
          <NotFound>
            <img alt="" src={sad} style={{ height: "80px" }}></img>
            <p>Nic nie znaleziono</p>
          </NotFound>
        ) : (
          <>
            <BeerList>
              {data.map((beer: Beer) => (
                <BeerCard key={beer.id} beer={beer} />
              ))}
              <PlaceHolder />
              <PlaceHolder />
              <PlaceHolder />
            </BeerList>
            <PaginationContainer>
              <IconButton onClick={handlePrev} disabled={page === 1}>
                <img src={left} alt="" width="20px" height="20px" />
              </IconButton>
              <PageNumber>{page}</PageNumber>
              <IconButton
                onClick={handleNext}
                disabled={data && !isFetching && data.length !== 12}
              >
                <img src={right} alt="" width="20px" height="20px" />
              </IconButton>
            </PaginationContainer>
          </>
        )}
      </BeerListContainer>
    </Container>
  )
}

export default Beers
