import { Beers, Details } from "modules"
import { Navigate } from "react-router-dom"

export const routesList = [
  {
    path: "/beers",
    children: [
      { index: true, element: <Beers /> },
      { path: ":beerId", element: <Details /> },
    ],
  },
  {
    path: "/",
    element: <Navigate to="/beers" replace />,
  },
  {
    path: "*",
    element: <>not found</>,
  },
]
