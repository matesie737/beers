import { SetStateAction, useState } from "react"
import { Container, Logo, Spacing, SearchContainer } from "./NavBar.styles"
import { IconButton, TextField, useMediaQuery } from "@mui/material"
import { useNavigate } from "react-router-dom"
import { logo, search } from "utils"

const NavBar = () => {
  const navigate = useNavigate()
  const [searchValue, setSearchValue] = useState("")

  const handleChange = (event: {
    target: { value: SetStateAction<string> }
  }) => {
    setSearchValue(event.target.value)
  }

  const handleSearch = () => {
    navigate({
      pathname: "/beers",
      search: searchValue.length > 0 ? `?search=${searchValue}` : "?",
    })
  }

  const handleMain = () => {
    navigate({
      pathname: "/beers",
    })
  }

  const handleKeyDown = (event: { keyCode: number }) => {
    if (event.keyCode === 13) handleSearch()
  }

  const isSmallScreen = useMediaQuery("(max-width:600px)")

  return (
    <>
      <Container sx={{ height: isSmallScreen ? "60px" : "80px" }}>
        <IconButton onClick={handleMain}>
          <Logo src={logo} sx={{ height: isSmallScreen ? "30px" : "50px" }} />
        </IconButton>
        <SearchContainer
          style={{
            width: isSmallScreen ? "90%" : "40%",
          }}
        >
          <TextField
            type="search"
            placeholder="Search"
            onChange={handleChange}
            fullWidth
            size={isSmallScreen ? "small" : "medium"}
            sx={{
              backgroundColor: "#FFFFFF",
              borderRadius: "4px",
            }}
            onKeyDown={handleKeyDown}
          />
          <IconButton onClick={handleSearch}>
            <img
              src={search}
              alt=""
              style={{ height: isSmallScreen ? "25px" : "40px" }}
            />
          </IconButton>
        </SearchContainer>
        <div
          style={{
            width: "10%",
            maxWidth: "60px",
            display: isSmallScreen ? "none" : "",
          }}
        />
      </Container>
      <Spacing />
    </>
  )
}

export default NavBar
