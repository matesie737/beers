import styled from "@emotion/styled"
import { Button, Divider as BaseDivider } from "@mui/material"

export const Container = styled("div")({
  width: "300px",
  height: "200px",
  margin: "25px",
  backgroundColor: "transparent",
  "&:hover>:first-of-type": {
    transform: "rotateY(180deg)",
  },
  perspective: "1000px",
})

export const Card = styled(Button)({
  position: "relative",
  width: "100%",
  height: "100%",
  border: "5px double #657787",
  color: "#000000",
  transition: "transform 0.6s",
  transformStyle: "preserve-3d",
  boxShadow: "0 4px 8px 0 rgba(0,0,0,0.2)",
})

export const BackDiv = styled("div")({
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
  transform: "rotateY(180deg)",
  backfaceVisibility: "hidden",
})
export const FrontDiv = styled("div")({
  width: "100%",
  height: "100%",
  position: "absolute",
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
  backfaceVisibility: "hidden",
})

export const Img = styled("img")({
  height: "80%",
  width: "40%",
  objectFit: "contain",
})

export const RightColumn = styled("div")({
  width: "60%",
  height: "100%",
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
})

export const Title = styled("div")({
  fontFamily: "'Sora', sans-serif",
  textAlign: "center",
  fontSize: "14px",
  fontWeight: "bold",
  color: "#333333",
  textShadow: "0px 0px 4px #777777",
})

export const TagLine = styled("div")({
  fontSize: "12px",
  width: "80%",
  textWrap: "balance",
})

export const CornerWraper = styled("div")({
  display: "flex",
  position: "absolute",
  top: "10px",
  right: "-27px",
  flexDirection: "column",
  alignItems: "center",
  transform: "rotate(45deg)",
})

export const UpperWrap = styled("div")({
  width: "45px",
  height: "2px",
  backgroundColor: "#657787",
})

export const LowerWrap = styled(UpperWrap)({
  width: "105px",
})

export const AbvValue = styled("div")({
  fontSize: "16px",
  color: "#333333",
  fontWeight: "500",
  textShadow: "0px 0px 2px black",
})

export const Divider = styled(BaseDivider)({
  "::before, ::after": {
    background: "#000000",
    width: "90px",
  },
})

export const PlaceHolder = styled("div")({
  height: "10px",
  width: "10px",
})

export const Star = styled("img")({
  height: "25px",
  width: "25px",
  display: "flex",
})
