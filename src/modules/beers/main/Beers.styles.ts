import styled from "@emotion/styled"
import { CircularProgress as BaseCircularProgress } from "@mui/material"

export const Container = styled("div")({
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
})

export const BeerListContainer = styled("div")({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  flexDirection: "column",
})

export const BeerList = styled("div")({
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
  alignContent: "space-around",
  justifyContent: "space-around",
  width: "calc(100vw-30px)",
  maxWidth: "1500px",
  margin: "0px 15px",
})

export const CircularProgress = styled(BaseCircularProgress)({
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%,-50%)",
})

export const PaginationContainer = styled("div")({
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  margin: "15px 0px 30px 0px",
})

export const PageNumber = styled("div")({})

export const PlaceHolder = styled("div")({
  width: "300px",
  margin: "0px 25px",
})

export const NotFound = styled("div")({
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%,-50%)",
  fontSize: "20px",
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
})
