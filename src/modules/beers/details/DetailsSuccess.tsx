import { Beer } from "types"
import {
  AbvIbu,
  Amount,
  Attribute,
  BottomContainer,
  Container,
  Description,
  ImageContainer,
  Ingredient,
  IngredientTypeTitle,
  IngridientsContainer,
  Name,
  ParametrContainer,
  RightContainer,
  Shadow,
  Tagline,
  Time,
  Title,
  UpperContainer,
} from "./Details.styles"
import { useMediaQuery } from "@mui/material"

export const BeerDetails = (props: { beer: Beer }) => {
  const { beer } = props

  const isTablet = useMediaQuery("(max-width:768px)")
  const isMobile = useMediaQuery("(max-width:425px)")

  return (
    <Container>
      <ImageContainer
        style={{
          width: isMobile ? "100%" : "40%",
          height: isMobile ? "" : "100%",
        }}
      >
        <img
          alt=""
          src={beer.image_url}
          style={{ width: "50%", height: "80%", objectFit: "contain" }}
        />
        <Shadow />
      </ImageContainer>
      <RightContainer
        style={{
          width: isMobile ? "100%" : "50%",
          minHeight: isMobile ? "" : "calc(100vh - 90px)",
          marginRight: isMobile ? "" : "20px",
        }}
      >
        <UpperContainer>
          <Title>{beer.name}</Title>
          <Tagline>{beer.tagline}</Tagline>
          <Description>{beer.description}</Description>
        </UpperContainer>
        <BottomContainer
          style={{
            flexDirection: isTablet ? "column" : "row",
            width: "90%",
          }}
        >
          <ParametrContainer
            style={{
              flexDirection: isTablet ? "row" : "column",
              width: isTablet ? "100%" : "20%",
              justifyContent: isTablet ? "space-around" : "center",
            }}
          >
            <AbvIbu
              style={{
                width: isTablet ? "40%" : "100%",
                height: isTablet ? "40px" : "40%",
                flexDirection: isTablet ? "row" : "column",
              }}
            >
              <div
                style={{
                  marginRight: isTablet ? "10px" : "0px",
                  fontWeight: "500",
                  fontSize: isTablet ? "16px" : "20px",
                }}
              >
                Abv:
              </div>
              <div>{beer.abv}%</div>
            </AbvIbu>
            <AbvIbu
              style={{
                width: isTablet ? "40%" : "100%",
                height: isTablet ? "40px" : "40%",
                flexDirection: isTablet ? "row" : "column",
              }}
            >
              <div
                style={{
                  marginRight: isTablet ? "10px" : "0px",
                  fontWeight: "500",
                }}
              >
                IBU:
              </div>
              <div>{beer.ibu}</div>
            </AbvIbu>
          </ParametrContainer>
          <IngridientsContainer
            style={{
              width: isTablet ? "calc(100% - 40px)" : "80%",
              justifyContent: isTablet ? "space-around" : "center",
              margin: isTablet ? "20px 0px" : "20px",
            }}
          >
            <div>
              <IngredientTypeTitle>Hops:</IngredientTypeTitle>
              {beer.ingredients.hops.map((ingredient, index) => {
                return (
                  <Ingredient key={index}>
                    {"•"}
                    <Amount>
                      {ingredient.amount.value + " " + ingredient.amount.unit}
                    </Amount>
                    <Name>{ingredient.name}</Name>
                    <Time>{"(" + ingredient.add + ") - "}</Time>
                    <Attribute>{ingredient.attribute}</Attribute>
                  </Ingredient>
                )
              })}
            </div>
            <div>
              <IngredientTypeTitle>Malt:</IngredientTypeTitle>
              {beer.ingredients.malt.map((ingredient, index) => {
                return (
                  <Ingredient key={index}>
                    {"•"}
                    <Amount>
                      {ingredient.amount.value + " " + ingredient.amount.unit}
                    </Amount>
                    <Name>{ingredient.name}</Name>
                  </Ingredient>
                )
              })}
            </div>
            <div>
              <IngredientTypeTitle>Yeast:</IngredientTypeTitle>
              <Ingredient>{beer.ingredients.yeast}</Ingredient>
            </div>
          </IngridientsContainer>
        </BottomContainer>
      </RightContainer>
    </Container>
  )
}
