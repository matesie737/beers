import { configureStore, combineReducers } from '@reduxjs/toolkit';
import { beerApi } from 'api';

const rootReducer = combineReducers({
  [beerApi.reducerPath]: beerApi.reducer,
})

export const store = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware => 
    getDefaultMiddleware()
    .concat(beerApi.middleware)
})