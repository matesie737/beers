import { styled } from "@mui/material"

export const Container = styled("div")({
  width: "100%",
  height: "70px",
  position: "fixed",
  top: "0px",
  zIndex: "10000",
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
  backgroundColor: "#102C44",
})

export const Spacing = styled("div")({
  width: "100%",
  height: "90px",
})

export const Logo = styled("img")({
  height: "50px",
  marginLeft: "10px",
})

export const SearchContainer = styled("div")({
  display: "flex",
  alignItems: "center",
})
