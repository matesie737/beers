import { BrowserRouter, useRoutes } from "react-router-dom"
import { routesList } from "./routesList"
import { NavBar } from "components"

const RoutesList = () => {
  const routes = useRoutes(routesList)
  return routes
}

export const Router = () => {
  return (
    <>
      <BrowserRouter>
        <NavBar />
        <RoutesList />
      </BrowserRouter>
    </>
  )
}
