import { Beer } from "types"
import {
  Container,
  Card,
  Img,
  RightColumn,
  Title,
  TagLine,
  CornerWraper,
  AbvValue,
  UpperWrap,
  LowerWrap,
  FrontDiv,
  BackDiv,
  Divider,
  PlaceHolder,
  Star,
} from "./BeerCard.styles"
import { star } from "utils"
import { useNavigate } from "react-router-dom"

const BeerCard = (props: { beer: Beer }) => {
  const { beer } = props
  const navigate = useNavigate()

  const handleNavigate = () => {
    navigate(`/beers/${beer.id}`)
  }

  return (
    <Container>
      <Card onClick={handleNavigate}>
        <FrontDiv>
          <Img src={beer.image_url} alt="" />
          <RightColumn>
            <CornerWraper>
              <UpperWrap />
              <AbvValue>{beer.abv + "%"}</AbvValue>
              <LowerWrap />
            </CornerWraper>
            <Title>{beer.name}</Title>
          </RightColumn>
        </FrontDiv>
        <BackDiv>
          <PlaceHolder />
          <Divider>
            <Star src={star} alt="" />
          </Divider>
          <TagLine>{beer.tagline}</TagLine>
        </BackDiv>
      </Card>
    </Container>
  )
}

export default BeerCard
