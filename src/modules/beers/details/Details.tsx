import { skipToken } from "@reduxjs/toolkit/dist/query"
import { useGetBeerByIdQuery } from "api"
import { useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import { CircularProgress, NotFound } from "./Details.styles"
import { sad } from "utils"
import { BeerDetails } from "./DetailsSuccess"

const Details = () => {
  const { beerId } = useParams()
  const [id, setId] = useState<number | typeof skipToken>(skipToken)
  const [notNumber, setNotNumber] = useState<boolean>(false)
  const { data, isLoading, isUninitialized } = useGetBeerByIdQuery(id)

  useEffect(() => {
    const number = Number(beerId)
    Number.isNaN(number) ? setNotNumber(true) : setId(number)
  }, [beerId])

  return (
    <>
      {isLoading || (isUninitialized && !notNumber) ? (
        <CircularProgress />
      ) : !data ? (
        <NotFound>
          <img alt="" src={sad} style={{ height: "80px" }}></img>
          <p>Nie znaleziono piwa o ID: {beerId}</p>
        </NotFound>
      ) : (
        <BeerDetails beer={data} />
      )}
    </>
  )
}

export default Details
