import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react"
import { Beer, Beers } from "types"

export const beerApi = createApi({
  reducerPath: "beer",
  baseQuery: fetchBaseQuery({
    baseUrl: "https://api.punkapi.com/v2/beers",
  }),
  endpoints: (builder) => ({
    getBeers: builder.query({
      query: (data: { page: number; search: string | null }) => ({
        url: `?page=${data.page}&per_page=12${
          data.search ? `&beer_name=${data.search}` : ""
        }`,
        method: "GET",
      }),
      transformResponse: (response: Beers) => response,
    }),
    getBeerById: builder.query({
      query: (beerId: number) => ({
        url: `/${beerId}`,
        method: "GET",
      }),
      transformResponse: (response: Beer[]) => response[0],
    }),
  }),
})

export const { useGetBeersQuery, useGetBeerByIdQuery } = beerApi
